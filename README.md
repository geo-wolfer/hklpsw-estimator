# hklpsw-estimator

Implementation of the estimator defined in:
Mixing Time Estimation in Reversible Markov Chains from a Single Sample Path
D. Hsu, A. Kontorovich, D. Levin, Y. Peres, C. Szepesvári, G. Wolfer - Annals of Applied Probability, 2019

## Bibtex


@article{hsu2019,
	author = "Hsu, Daniel and Kontorovich, Aryeh and Levin, David A. and Peres, Yuval and Szepesv\'{a}ri, Csaba and Wolfer, Geoffrey",
	doi = "10.1214/18-AAP1457",
	fjournal = "The Annals of Applied Probability",
	journal = "Ann. Appl. Probab.",
	month = "08",
	number = "4",
	pages = "2439--2480",
	publisher = "The Institute of Mathematical Statistics",
	title = "Mixing time estimation in reversible Markov chains from a single sample path",
	url = "https://doi.org/10.1214/18-AAP1457",
	volume = "29",
	year = "2019"
}