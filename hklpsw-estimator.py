import numpy as np
from scipy.sparse.linalg import eigsh


def add_rev_spectral_gap(d, X, alpha=0):
    N = np.bincount(X[:-1], minlength=d) + d * alpha
    flat_coordinates = np.ravel_multi_index((X[:-1], X[1:]), (d, d))
    T = np.mat(np.bincount(flat_coordinates, minlength=d * d).reshape((d, d))) + alpha
    sqrtDinv = np.mat(np.diag(np.power(2 * N, -1 / 2)))
    A = (sqrtDinv * (T + T.T) * sqrtDinv)
    eigenvalues, _ = eigsh(A, k=2)
    lambda_star = np.min(np.abs(eigenvalues))
    return 1 - lambda_star


def main():
    # d: number of states
    d = 3

    # X: trajectory on the Markov chain
    X = [2, 0, 1, 0, 2, 2, 0, 2, 2, 1,
         0, 2, 2, 2, 2, 0, 1, 0, 2, 0,
         2, 2, 2, 2, 2, 0, 2, 2, 2, 2,
         2, 2, 0, 2, 0, 2, 0, 2, 0, 2,
         2, 0, 2, 2, 2, 2, 2, 0, 2, 0,
         2, 0, 1, 2, 2, 2, 0, 2, 2, 0,
         2, 2, 2, 2, 2, 2, 0, 2, 2, 2,
         0, 1, 0, 2, 2, 0, 1, 0, 2, 0,
         1, 2, 0, 2, 2, 0, 2, 0, 1, 2,
         2, 2, 2, 0, 2, 2, 2, 2, 2, 0]

    # alpha: smoothing parameter
    alpha = 0.001

    # estimator
    asg_est = add_rev_spectral_gap(d, X, alpha)


if __name__ == "__main__":
    main()
